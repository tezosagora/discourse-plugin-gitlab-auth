# frozen_string_literal: true

# name:  discourse-plugin-gitlab-auth
# about: Enable Login via GitLab
# version: 0.0.1
# author: Francis Brunelle
# url: https://gitlab.com/tezosagora/discourse-plugin-gitlab-auth

require 'auth/oauth2_authenticator'

gem 'omniauth-gitlab', '2.0.0'

register_svg_icon "fab-gitlab" if respond_to?(:register_svg_icon)

enabled_site_setting :gitlab_enabled

class GitLabAuthenticator < ::Auth::OAuth2Authenticator
  PLUGIN_NAME = 'oauth-gitlab'

  def name
    'gitlab'
  end

  def enabled?
    SiteSetting.gitlab_enabled
  end

  def can_revoke?
    true
  end

  def revoke(user, skip_remote: false)
    info = Oauth2UserInfo.find_by(user_id: user.id, provider: name)
    raise Discourse::NotFound if info.nil?
    info.destroy!
    true
  end

  def after_authenticate(auth_token)
    result = super

    if result.user && result.email && (result.user.email != result.email)
      begin
        result.user.primary_email.update!(email: result.email)
      rescue
        used_by = User.find_by_email(result.email)&.username
        Rails.loger.warn("FAILED to update email for #{user.username} to #{result.email} cause it is in use by #{used_by}")
      end
    end

    result
  end

  def register_middleware(omniauth)
    omniauth.provider :gitlab,
                      scope: 'read_user',
                      setup: lambda { |env|
                        strategy = env['omniauth.strategy']
                        strategy.options[:client_id] = SiteSetting.gitlab_client_id
                        strategy.options[:client_secret] = SiteSetting.gitlab_secret
                      }
  end
end

auth_provider icon: 'fab-gitlab',
              frame_width: 920,
              frame_height: 800,
              authenticator: GitLabAuthenticator.new(
                'gitlab',
                trusted: true,
                auto_create_account: true
              )

register_css <<CSS

.btn-social.gitlab {
  background: #e14027;
}

CSS
